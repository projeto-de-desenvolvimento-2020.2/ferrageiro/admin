import Vue from 'vue'
import VueRouter from 'vue-router'
import Index from './components/Index.vue'
import Inicial from './components/Inicial.vue'
//fornecedor
import DetalhesFornecedor from './components/DetalhesFornecedor.vue'
import CadastroFornecedor from './components/CadastroFornecedor.vue'
//marca categoria
import CadastroMarca from './components/CadastroMarca.vue'
import CadastroCategoria from './components/CadastroCategoria.vue'
//produto
import CadastroProduto from './components/CadastroProduto.vue'
import ProdutosDestaque from './components/ProdutosDestaque.vue'
import ListagemProdutos from './components/ListagemProdutos.vue'
import RelatorioListagemProdutos from './components/RelatorioListagemProdutos.vue'
//cliente
import DetalhesCliente from './components/DetalhesCliente.vue'
import CadastroCliente from './components/CadastroCliente.vue'
import ListagemClientes from './components/ListagemClientes.vue'
import RelatorioListagemClientes from './components/RelatorioListagemClientes.vue'
//pedido
import AbrirPedido from './components/AbrirPedido.vue'
import PedidosAberto from './components/PedidosAberto.vue'
import ListagemPedidos from './components/ListagemPedidos.vue'
import Pedido from './components/Pedido.vue'
import PedidosEntregues from './components/PedidosEntregues.vue'

Vue.use(VueRouter)
export default new VueRouter({
    mode: 'history',
    routes: [
        // Unicas
        { path: '/', component: Index },
        { path: '/inicial', component: Inicial },
        //Fornecedor
        { path: '/cadastrofornecedor', name: 'cadastro-de-fornecedor', component: CadastroFornecedor },
        { path: '/detalhesfornecedor/:id', name: 'datalhes-dos-fornecedores', component: DetalhesFornecedor },
        //{ path: '*', name: 'error404', compornent: error404},
        //Categoria e Marca
        { path: '/cadastromarca', name: 'nova-marca', component: CadastroMarca },
        { path: '/cadastrocategoria', name: 'nova-categoria', component: CadastroCategoria },
        //Produtos
        { path: '/destaques', name: 'produtos-em-destaque', component: ProdutosDestaque },
        { path: '/cadastroproduto', name: 'novo-produto', component: CadastroProduto },
        { path: '/listagemprodutos', name: 'listagem-de-produtos', component: ListagemProdutos },
        //Clientes
        { path: '/clientes/:id', name: 'detalhes-cliente', component: DetalhesCliente },
        { path: '/cadastrocliente', name: 'novo-cliente', component: CadastroCliente },
        { path: '/listagemclientes', name: 'listagem-de-clientes', component: ListagemClientes },
        //Pedidos
        { path: '/pedido/:id', name: 'pedido', component: Pedido},
        { path: '/abrirpedido', name: 'novo-pedido', component: AbrirPedido },
        { path: '/pedidosemaberto', name: 'pedidos-em-aberto', component: PedidosAberto},
        { path: '/listagempedidos', name: 'listagem-de-pedidos', component: ListagemPedidos },
        { path: '/pedidosentregues', name: 'pedidos-entregues', component: PedidosEntregues },

        //Relatórios
        { path: '/relatorios/listagemclientes', name: 'relatorio-listagem-clientes', component: RelatorioListagemClientes },
        { path: '/relatorios/listagemprodutos', name: 'relatorio-listagem-produtos', component: RelatorioListagemProdutos },
    ]
})